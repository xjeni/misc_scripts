#!/usr/bin/env perl




## MODULES
use strict;
no warnings;
use Bio::SeqIO;
use File::Find;
use File::Spec;
use Bio::DB::GenBank;
use Getopt::Long;
use FindBin;

## VARIABLES
my $query_file;
my $out_file;
my $err_help = "To see help doc, use command \"perl getSeqCountGenBank.pl -h\"";

## SETUP
&setup;
print "Using query file: $query_file\n";
print "Using output file path: $out_file\n";
print "START: " . localtime . "\n";


open (QUERY, "<", $query_file) or die $!;
open (OUT, ">", "$out_file") or die $!;
while (my $line = <QUERY>) {
  chomp $line;
  my @line_val = split(/\t/, $line);
  my $name = $line_val[0];
  my $query = $line_val[1];
  print $name, "\n";

  my $count;
  my $err_str;
  open(my $err, ">", \$err_str) or die $!;
  my $query_genbank = Bio::DB::Query::GenBank->new (-query   => "$query",
                                                    -db      => 'nucleotide');
  do {
    local *STDERR = $err;
    eval {$query_genbank->count};
  };
  if ($@ eq ""){
    $count = $query_genbank->count;
  } else {
    $count = "0";
  }
  print OUT $name, "\t", $count,"\n";
}



close QUERY;
close OUT;


print "END: " . localtime . "\n";






# SETUP OPTIONS AND DEFAULT VALUES
sub setup{
  my $help;
  GetOptions("--help" => \$help, 
             "-help" => \$help, 
             "-h" => \$help, 
             "-query=s" => \$query_file,
             "-out=s" => \$out_file) or die $!;
  if (defined $help){
    # prints out the help doc
    system("cat $FindBin::Bin/help_getSeqCountGenBank.txt");
    exit;
  }

  my $err = 0;

  ## Checking if mandatory fields are given
  if (! defined $query_file){
    print "Error: query file path is not provided. (-query [query_file])\n";
    $err = 1;
  } elsif (! -e $query_file){
    print "Error: the provided query file path does not exist.\n";
    print "    Input: $query_file\n";
    $err = 1;
  }
  if (! defined $out_file){
    print "Error: output file path is not provided. (-out [seq_count_file])\n";
    $err = 1;
  }
  # If input errors found, then exit script
  if ($err){
    print "($err_help)\n";
    exit;
  }
}
