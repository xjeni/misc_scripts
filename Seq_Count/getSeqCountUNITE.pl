#!/usr/bin/env perl

## MODULES
use strict;
use warnings;
use FindBin;
use Getopt::Long;

## VARIABLES
my $names_file;
my $out_file;
my $unite_fasta;
my $rank;
my $rank_letter;
my $err_help = "To view help doc, use command \"perl getSeqCountUNITE.pl -h\"";

## SETUP WITH COMMANDLINE OPTIONS
&setup;

## START OF SCRIPT
print "START: " . localtime . "\n";

open(READ, "<", $names_file) or die $!;
open(OUT, ">", $out_file) or die $!;
while(my $name = <READ>){
  chomp $name;
  $name =~ s/\R//g;
  print $name, "\n";
  my $grep = "grep \"" . 
             "$rank_letter\__$name\$\\|" . 
             "$rank_letter\__$name|\\|" . 
             "$rank_letter\__$name;\" " . 
             "$unite_fasta | wc -l";
  my $seq_count = `$grep`;
  chomp $seq_count;
  print OUT $name, "\t", $seq_count, "\n";
}
close OUT;
close READ;

print "END: " . localtime . "\n";
## END OF SCRIPT






# SETUP OPTIONS AND DEFAULT VALUES
sub setup{
  my $help;
  GetOptions("--help" => \$help, 
             "-help" => \$help, 
             "-h" => \$help, 
             "-names=s" => \$names_file,
             "-rank=s" => \$rank,
             "-unite=s" => \$unite_fasta,
             "-out=s" => \$out_file) or die $!;
  if (defined $help){
    # prints out the help doc
    system("cat $FindBin::Bin/help_getSeqCountUNITE.txt");
    exit;
  }

  my $err = 0;

  ## Checking if mandatory fields are given
  if (! defined $names_file){
    print "Error: names file path is not provided. (-names [names_file])\n";
    $err = 1;
  } elsif (! -e $names_file){
    print "Error: the provided names file path does not exist.\n";
    print "    Input: $names_file\n";
    $err = 1;
  }
  if (! defined $out_file){
    print "Error: output file path is not provided. (-out [seq_count_file])\n";
    $err = 1;
  }
  if (! defined $rank){
    print "Error: the rank is not provided. (-rank [rank_of_names])\n";
    $err = 1;
  } elsif (($rank ne 'kingdom') &&
           ($rank ne 'phylum') &&
           ($rank ne 'class') &&
           ($rank ne 'order') &&
           ($rank ne 'family') &&
           ($rank ne 'genus') &&
           ($rank ne 'species') &&
           ($rank ne 'k') &&
           ($rank ne 'p') &&
           ($rank ne 'c') &&
           ($rank ne 'o') &&
           ($rank ne 'f') &&
           ($rank ne 'g') &&
           ($rank ne 's')){
    print "Error: the rank provided is invalid. See help doc for the list of valid inputs.\n";
    $err = 1;
  }
  if (! defined $unite_fasta){
    print "Error: unite fasta path is not provided. (-unite [unite_fasta_path])\n";
    $err = 1;
  } elsif (! -e $unite_fasta){
    print "Error: the provided unite fasta path does not exist.\n";
    print "    Input: $unite_fasta\n";
    $err = 1;
  }
  # If invalid input found, exit script
  if ($err){
    print "($err_help)\n";
    exit;
  }
  $rank_letter = substr($rank, 0, 1);
}
