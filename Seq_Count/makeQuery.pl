#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long;
use FindBin;

## VARIABLES
my $names_file;
my $out_file;
my $query_string;
my $err_help = "To see help doc, use command \"perl makeQuery.pl -h\"";

## SETUP
&setup;
print "Using names file: $names_file\n";
print "Using output file path: $out_file\n";
print "START: " . localtime . "\n";

open(READ, "<", $names_file) or die $!;
open(OUT, ">", $out_file) or die $!;
while (my $name = <READ>){
  chomp $name;
  $name =~ s/\R//g;
  my $id = $name;
  $id =~ s/ /_/g;
  print OUT $id, "\t", "\"$name\"", $query_string, "\n";
}
close READ;
close OUT;

print "END: " . localtime . "\n";








# SETUP OPTIONS AND DEFAULT VALUES
sub setup{
  my $help;
  GetOptions("--help" => \$help, 
             "-help" => \$help, 
             "-h" => \$help, 
             "-names=s" => \$names_file,
             "-phrase=s" => \$query_string,
             "-out=s" => \$out_file) or die $!;
  if (defined $help){
    # prints out the help doc
    system("cat $FindBin::Bin/help_makeQuery.txt");
    exit;
  }

  my $err = 0;

  ## Checking if mandatory fields are given
  if (! defined $names_file){
    print "Error: names file path is not provided. (-names [names_file_phrase])\n";
    $err = 1;
  } elsif (! -e $names_file){
    print "Error: the provided names file path does not exist.\n";
    print "    Input: $names_file\n";
    $err = 1;
  }
  if (! defined $out_file){
    print "Error: output file path is not provided. (-out [query_file_path])\n";
    $err = 1;
  }
  if (! defined $query_string){
    print "Error: phrase to use to make the queries is not provided. (-phrase [query_phrase])\n";
    $err = 1;
  }
  # If input errors found, then exit script
  if ($err){
    print "($err_help)\n";
    exit;
  }
}
