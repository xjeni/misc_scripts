=========================
  GenBank/makeQuery.pl
=========================

This script takes a list of taxonomic names, a query phrase, and make a query file
  containing all the taxonomic names and their respective query strings. It was
  originally used to make the query file for the script "getSeqCount.pl".
  For example, if given a [names_file] containing:
    Aspergillus
    Fusarium
    Penicillium
  And a [query_phrase]:
    '[orgn] AND fungi[filter]'
  Then the script will produce the following output [query_file]:
    Aspergillus		"Aspergillus"[orgn] AND fungi[filter]
    Fusarium		"Fusarium"[orgn] AND fungi[filter]
    Penicillium		"Penicillium"[orgn] AND fungi[filter]

=========================
  Usage:
=========================

perl makeQuery.pl -names [names_file] -out [query_file] -phrase [query_phrase]

To print out the help doc, use any of the following:
  perl makeQuery.pl -h
  perl makeQuery.pl -help
  perl makeQuery.pl --help

MANDATORY FIELDS:
    -names [names_file]		Path of names file. See "Example/example_names.txt"
				  for example. Each line contains one taxonomic
				  name.
    -out [query_file]		Tab-delimited output file containing query created
				  by concatenating the name and the [query_phrase]
				  together. The columns are:
				    1. Unique query ID
				    2. Query
				  The unique query ID is based on the taxonomic
				    name, with space replaced by underscrolls.
				  See "Example/example_query.tab" for example of
				    the output file.

=========================
  Example:
=========================

perl makeQuery.pl -names Example/example_names.txt -out query.tab -phrase '[orgn] AND fungi[filter]'

- The output, "query.tab", will be created in the current working directory
