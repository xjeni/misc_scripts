=========================
  getSeqCountUNITE.pl
=========================

This script obtains the number of sequences in an UNITE fasta file for a list of
  taxon names of the same rank, using the lineage that is in the UNITE fasta file 
  headers.

=========================
  Usage:
=========================

perl getSeqCountUNITE.pl -query [query_file] -out [seq_count_file] -rank [taxon_rank] -unite [unite_fasta_path]

To print out the help doc, use any of the following:
  perl getSeqCountUNITE.pl -h
  perl getSeqCountUNITE.pl -help
  perl getSeqCountUNITE.pl --help

MANDATORY FIELDS:
  -names [names_file]		Path of names file. See "Example/example_names.txt" 
				  for example. It is a tab-delimited file, with
				  one taxon name on each line. The script will obtain
                                  the number of sequences the input UNITE fasta file
                                  for each name in the names file.
  -out [seq_count_file]		Tab-delimited output file containing the 
				  number of sequences in GenBank for the given
				  list of queries.
				  The columns are:
				    1. Taxon name
				    2. Sequence count
                                  See "Example/example_seqcount.tab" for example
				    of the output file.
  -unite [unite_fasta_path]	Path to UNITE fasta file, which has the sequence 
                                  taxonomic lineage in fasta header. The UNITE fasta 
                                  can be download here: 
                                  https://unite.ut.ee/repository.php
  -rank [taxon_rank]		Rank of the names in [names_file]. Available input:
				  - kingdom
				  - phylum
				  - class
				  - order
				  - family
				  - genus
				  - species
                        	  Note: The first letters can also be used. 
                                    (I.e. 'k', 'p', 'c', etc.)

=========================
  Example:
=========================

perl getSeqCountUNITE.pl -names Example/example_names.txt -out unite_seqcount.tab -rank g -unite [unite_fasta]

- Replace [unite_fasta] with your UNITE fasta path
- The output, "unite_seqcount.tab", will be created in the current working directory
