#!/usr/bin/env perl
use strict;
use warnings;

my $sum_dir = shift or die $!;
my $out_sum = shift or die $!;
my $seq_count_file = shift or die $!;
open(READ, "<", $seq_count_file) or die $!;
my %seq_count;
while (my $line = <READ>){
  chomp $line;
  $seq_count{ (split(/\t/, $line))[0] } = (split(/\t/, $line))[1];
}
close READ;

opendir(SUM_DIR, $sum_dir) or die $!;
open(OUT, ">", "$out_sum.1.temp") or die $!;

while (my $file = readdir(SUM_DIR)){
  next if ($file =~ /^\./);
  my @filename = split(/_/, $file);
  my $qfile = $filename[0];
  my $dbfile = $filename[1];
  my $conf = $filename[3];
  my $qseq_count = $seq_count{$qfile} or 
                   die "Error: $qfile not found in $seq_count_file";
  my $dbseq_count = $seq_count{$dbfile} or 
                    die "Error: $dbfile not found in $seq_count_file";

  my $same_lin_count = 0;
  my $diff_lin_count = 0;

  my $asn_gen_count = 0;
  my $unasn_gen_count = 0;
  my $same_id_gen_count = 0;
  my $diff_id_gen_count = 0;
  my $all_id_gen_count = 0;
  my $same_unid_gen_count = 0;
  my $diff_unid_gen_count = 0;
  my $all_unid_gen_count = 0;

  my $asn_spec_count = 0;
  my $unasn_spec_count = 0;
  my $same_id_spec_count = 0;
  my $diff_id_spec_count = 0;
  my $all_id_spec_count = 0;
  my $same_unid_spec_count = 0;
  my $diff_unid_spec_count = 0;
  my $all_unid_spec_count = 0;

  open(READ, "<", "$sum_dir/$file") or die $!;
  <READ>;
  while (my $line = <READ>){
    chomp $line;
    my @line_vals = split(/\t/, $line);
    if (@line_vals < 11){
      die "Error: Invalid number of columns read. Line:\n$line\n";
    }
    my $lin_comp = $line_vals[4];
    if ($lin_comp eq "Same_Lin"){
      $same_lin_count++;
    } elsif ($lin_comp eq "Diff_Lin"){
      $diff_lin_count++;
    } else {
      warn "Warning: Invalid value read for LIN_COMP column: $lin_comp\n";
    }

    my $gen_comp = $line_vals[7];
    if ($gen_comp eq 'Unasn_Gen'){
      $unasn_gen_count++;
    } elsif ($gen_comp eq 'Same_Id_Gen'){
      $same_id_gen_count++;
    } elsif ($gen_comp eq 'Diff_Id_Gen'){
      $diff_id_gen_count++;
    } elsif ($gen_comp eq 'Same_Unid_Gen'){
      $same_unid_gen_count++;
    } elsif ($gen_comp eq 'Diff_Unid_Gen'){
      $diff_unid_gen_count++;
    } else {
      warn "Warning: Invalid value read for GEN_COMP column: $gen_comp\n";
    }

    my $spec_comp = $line_vals[10];
    if ($spec_comp eq 'Unasn_Spec'){
      $unasn_spec_count++;
    } elsif ($spec_comp eq 'Same_Id_Spec'){
      $same_id_spec_count++;
    } elsif ($spec_comp eq 'Diff_Id_Spec'){
      $diff_id_spec_count++;
    } elsif ($spec_comp eq 'Same_Unid_Spec'){
      $same_unid_spec_count++;
    } elsif ($spec_comp eq 'Diff_Unid_Spec'){
      $diff_unid_spec_count++;
    } else {
      warn "Warning: Invalid value read for SPEC_COMP column: $spec_comp\n";
    }
  }
  $all_id_gen_count = $same_id_gen_count + $diff_id_gen_count;
  $all_unid_gen_count = $same_unid_gen_count + $diff_unid_gen_count;
  $asn_gen_count = $qseq_count - $unasn_gen_count;

  $all_id_spec_count = $same_id_spec_count + $diff_id_spec_count;
  $all_unid_spec_count = $same_unid_spec_count + $diff_unid_spec_count;
  $asn_spec_count = $qseq_count - $unasn_spec_count;

  print OUT $qfile, "\t", $dbfile, "\t", $conf, "\t",
            $qseq_count, "\t", $dbseq_count, "\t",
            $same_lin_count, "\t", $diff_lin_count, "\t",

            $asn_gen_count, "\t", $unasn_gen_count, "\t", 
            $all_id_gen_count, "\t", $same_id_gen_count, "\t", $diff_id_gen_count, "\t", 
            $all_unid_gen_count, "\t", $same_unid_gen_count, "\t", $diff_unid_gen_count,"\t",

            $asn_spec_count, "\t", $unasn_spec_count, "\t", 
            $all_id_spec_count, "\t", $same_id_spec_count, "\t", $diff_id_spec_count, "\t", 
            $all_unid_spec_count, "\t", $same_unid_spec_count, "\t", $diff_unid_spec_count, "\n";
}

close OUT;
close SUM_DIR;

## COLUMN HEADERS
my $header = "QUERY\tDB\tCONF\t" .
             "QSEQ\tDBSEQ\t" .
             "SAME_LIN\tDIFF_LIN\t" .
             "ASN_GEN\tUNASN_GEN\t" .
             "ALL_ID_GEN\tSAME_ID_GEN\tDIFF_ID_GEN\t" .
             "ALL_UNID_GEN\tSAME_UNID_GEN\tDIFF_UNID_GEN\t" .
             "ASN_SPEC\tUNASN_SPEC\t" .
             "ALL_ID_SPEC\tSAME_ID_SPEC\tDIFF_ID_SPEC\t" .
             "ALL_UNID_SPEC\tSAME_UNID_SPEC\tDIFF_UNID_SPEC";

system("sort -n -k3 $out_sum.1.temp | sort -k2 | sort -k1 > $out_sum.2.temp");
system("> $out_sum");
system("echo \"$header\" >> $out_sum");
system("cat $out_sum.2.temp >> $out_sum");

system("rm $out_sum.*.temp");
