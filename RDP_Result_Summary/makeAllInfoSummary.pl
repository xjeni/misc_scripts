#!/usr/bin/env perl
use strict;
use warnings;

my $tax_asn_dir = shift or die $!;
my $id_taxon = shift or die $!;
my $gen_unid = shift or die $!;
my $spec_unid = shift or die $!;
my $seq_count_file = shift or die $!;
my $out_dir = shift or die $!;
my $sum_out_file = shift or die $!;

print "START: " . localtime . "\n";

opendir (DIR, $tax_asn_dir) or die $!;
while (my $file = readdir(DIR)){
  next if ($file =~ m/^\./);
  my $out_file = rmExt($file) . "_modified.tab";
  print "Running addInfo.pl on $file...\n";
  system("perl makeInfo.pl $tax_asn_dir/$file $out_dir/$out_file $id_taxon $gen_unid $spec_unid");
}
closedir(DIR);

print "Making summary file \"$sum_out_file\"...\n";
system("perl makeSummary.pl $out_dir $sum_out_file $seq_count_file");

print "END: " . localtime . "\n";

sub rmExt{
  my $filename = shift or die $!;
  my @chunks = split(/\./, $filename);
  my $chunks_size = @chunks;
  my $new_name = "";
  for (my $index = 0; $index < $chunks_size - 1; $index++){
    $new_name = $new_name . '.' . $chunks[$index];
  }
  substr $new_name, 0, 1, '';
  return $new_name;
}
