#!/usr/bin/env perl

use strict;
use warnings;

my $in_tax_file = shift or die $!;
my $out_file = shift or die $!;
my $seqid_taxon_file = shift or die $!;
my $gen_unid_file = shift or die $!;
my $spec_unid_file = shift or die $!;
my %id_taxon;
open(ID_TAXON, "<", $seqid_taxon_file) or die $!;
while(<ID_TAXON>){
  chomp;
  $id_taxon{ (split(/\t/, $_))[0] } = (split(/\t/, $_))[1];
}

## ADDING 'UNIDENTIFIED' PATTERN KEYWORDS TO ARRAY
my @gen_unid;
my @spec_unid;
open(GEN_PATTERN, "<", $gen_unid_file) or die $!;
while (<GEN_PATTERN>){
  chomp;
  push(@gen_unid, $_);
}
open(SPEC_PATTERN, "<", $spec_unid_file) or die $!;
while (<SPEC_PATTERN>){
  chomp;
  push(@spec_unid, $_);
}

open(OUT, ">", $out_file) or die $!;
open(READ, "<", $in_tax_file) or die $!;

## COLUMN HEADER
print OUT "ID\tCONF\t",
          "ORIG_LIN\tASN_LIN\tLIN_COMP\t",
          "ORIG_GEN\tASN_GEN\tGEN_COMP\t",
          "ORIG_SPEC\tASN_SPEC\tSPEC_COMP\n";

while (my $line = <READ>){
  chomp $line;
  my $seqid = (split(/\t/, $line))[0];
  my $conf = (split(/\t/, $line))[2];
  ## CHECK IF LIN MATCH
  my $lin_matched;
  my $orig_lin = $id_taxon{$seqid} or die "ERROR. GI NOT FOUND: $seqid\n";
  my $new_lin = (split(/\t/, $line))[1];
  if ($orig_lin eq $new_lin){
    $lin_matched = "Same_Lin";
  } else {
    $lin_matched = "Diff_Lin";
  }

  ## CHECK IF GEN MATCH
  my $orig_gen = (split(';', $orig_lin))[5];
  my $new_gen = (split(';', $new_lin))[5];
  my $gen_matched;
  if (! defined $new_gen){
    $new_gen = 'g__unassigned';
    $gen_matched = 'Unasn_Gen';
  } elsif ($orig_gen eq $new_gen){
    $gen_matched = 'Same_Id_Gen';
  } else {
    $gen_matched = 'Diff_Id_Gen';
  }

  foreach my $unid_pattern(@gen_unid){
    if (($orig_gen =~ /$unid_pattern/) ||
        ($new_gen =~ /$unid_pattern/)){
      if ($gen_matched eq 'Same_Id_Gen'){
        $gen_matched = 'Same_Unid_Gen';
      } else {
        $gen_matched = 'Diff_Unid_Gen';
      }
    }
  }

  ## CHECK IF SPEC MATCH
  my $spec_matched;
  my $orig_spec = (split(';', $orig_lin))[6];
  my $new_spec = (split(';', $new_lin))[6];
  if (! defined $new_spec){
    $new_spec = 's__unassigned';
    $spec_matched = 'Unasn_Spec';
  } else {
    my @new_spec_arr = split('_', $new_spec);
    my @orig_spec_arr = split('_', $orig_spec);
    $spec_matched = 'Same_Id_Spec';
    if ((scalar @new_spec_arr) ne (scalar @orig_spec_arr)){
      $spec_matched = 'Diff_Id_Spec';
    } else {
      my $len = @new_spec_arr;
      for (my $index = 0; $index < $len; ++$index){
        if ($new_spec_arr[$index] ne $orig_spec_arr[$index]){
          $spec_matched = 'Diff_Id_Spec';
        }
      }
    }
    foreach my $unid_pattern(@spec_unid){
      if (($orig_spec =~ /$unid_pattern/) ||
          ($new_spec =~ /$unid_pattern/)){
        if ($spec_matched eq 'Diff_Id_Spec'){
          $spec_matched = 'Diff_Unid_Spec';
        } else {
          $spec_matched = 'Same_Unid_Spec';
        }
      }
    }
  }


  ## PRINTING
  print OUT $seqid, "\t", $conf, "\t",  
            $orig_lin, "\t", $new_lin, "\t", $lin_matched, "\t", 
            $orig_gen, "\t", $new_gen, "\t", $gen_matched, "\t",
            $orig_spec, "\t", $new_spec, "\t", $spec_matched, "\n";
}
