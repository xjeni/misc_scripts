#!/usr/bin/env perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/Modules/Text-Unaccent-PurePerl-0.05/lib";
use Text::Unaccent::PurePerl qw(unac_string);
use Bio::DB::GenBank;
use Getopt::Long;

## VARIABLES
my $in_file;
my $out_file;
my $err_help = "To see help doc, use command: \"perl hasSeqs.pl -h\"";

## SETUP VIA COMMANDLINE CONFIG
&setup;

## PRINT OUT INFO
print "Using input file: $in_file\n";
print "Using output file: $out_file\n";

## START
print "START: " . localtime . "\n";

open(READ, "<", $in_file) or die $!;
open(OUT, ">", $out_file) or die $!;
while (my $line = <READ>){
  chomp $line;
  my $prot = unac_string((split(/\t/, $line))[0]);
  my $supp = unac_string((split(/\t/, $line))[1]);
  $prot =~ s/\R//g;
  $supp =~ s/\R//g;
  my $prot_seq = &hasSeqs($prot);
  my $supp_seq = &hasSeqs($supp);
  print OUT $prot, "\t", $supp, "\t", $prot_seq, "\t", $supp_seq, "\n";
}
close READ;
close OUT;

## END
print "END: " . localtime . "\n";


sub hasSeqs{
  my $gen = shift or die $!;
  my $query = "\"$gen\"\[orgn\] AND fungi\[filter\]";
  my $count;
  my $err_str;
  open(my $err, ">", \$err_str) or die $!;
  my $query_genbank = Bio::DB::Query::GenBank->new (-query   => "$query",
                                                    -db      => 'nucleotide');
  do {
    local *STDERR = $err;
    eval {$query_genbank->count};
  };
  if ($@ eq ""){
    $count = $query_genbank->count;
  } else {
    $count = "0";
  }


  return $count;
}

sub setup{
  my $help;
  GetOptions("--help" => \$help, 
             "-help" => \$help, 
             "-h" => \$help, 
             "-in=s" => \$in_file,
             "-out=s" => \$out_file) or die $!;
  if (defined $help){
    # prints out the help doc
    system("cat $FindBin::Bin/help_hasSeqs.txt");
    exit;
  }

  my $err = 0;

  ## Checking if mandatory fields are given
  if (! defined $in_file){
    print "Error: the input file path is not provided. (-in [input_file_path])\n";
    $err = 1;
  } elsif (! -e $in_file){
    print "Error: the provided input file path does not exist.\n";
    print "    Input: $in_file\n";
    $err = 1;
  }
  if (! defined $out_file){
    print "Error: the output file path is not provided. (-out [output_file_path])\n";
    $err = 1;
  }
  # If input errors found, then exit script
  if ($err){
    print "($err_help)\n";
    exit;
  }
}
