#!/usr/bin/env perl
use strict;
use warnings;

my $in_file = shift or die $!;
my $out_file = shift or die $!;

open(READ, "<", $in_file) or die $!;
open(OUT, ">", $out_file) or die $!;
while (my $line = <READ>){
  if ($line =~ /(\([0-9][0-9][0-9][0-9]\))/) {
    my $year = $1;
    substr $year, 0, 1, '';
    chop $year;
    print OUT $year, "\n";
  } else {
    print OUT "not_found\n";
  }
}
close OUT;
close READ;
