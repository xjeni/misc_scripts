#!/usr/bin/env perl
use strict;
use warnings;

## Input: output of taxidToLin.pl
## Output: modified version, to make it more readable; 
##         taxon id in first column, 
##         genus in second column, 
##         lineage in third column, 
##         "s__" part removed

my $in_file = shift or die $!;
my $out_file = shift or die $!;
my $rank = shift or die $!;

open(READ, "<", $in_file) or die $!;
open(OUT, ">", $out_file) or die $!;
while (my $line = <READ>){
  chomp $line;
  my $name;
  my @line_vals = split(/\t/, $line);
  my $lin = $line_vals[1];
  my $rank_char = substr($rank, 0, 1);
  if ($lin =~ /; $rank_char\__(.*)/){
    $name = $1;
  } else {
    die "ERROR:\n" . $line . "\n";
  }
  my $taxid = $line_vals[0];
  print OUT $taxid, "\t", $name, "\t", $lin, "\n";
}
close READ;
close OUT;
