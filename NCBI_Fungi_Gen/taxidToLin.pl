#!/usr/bin/env perl
use strict;
use warnings;
use lib "Modules";
use Bio::DB::Taxonomy;

## Input: Output of getAllFungalGenusTaxonIds.pl, 
##        which is a list of taxonomy ids
## Output: Tab-delimited file containing 
##         a column of taxon ids and 
##         their respective lineage in the second column

my $taxid_list = shift or die $!;
my $out_file = shift or die $!;
my $taxa_dir = shift or die $!;

open(READ, "<", $taxid_list) or die $!;
if (! -e "$taxa_dir/names2id"){
      print "It appears that the index does not already exist; this may take some time..." . localtime . "\n";
}

my $nodes = "$taxa_dir/nodes.dmp";
my $names = "$taxa_dir/names.dmp";

my $dbh = new Bio::DB::Taxonomy (-source => "flatfile",
                                 -directory => $taxa_dir,
                                 -nodesfile => $nodes,
                                 -namesfile => $names,
                                );
my @taxid_array = `cat $taxid_list`;

open(OUT, ">", $out_file) or die $!;
while (my $taxid = <READ>){
  chomp $taxid;

  my $taxon = $dbh->get_taxon(-taxonid => $taxid);
  my $tree;
  if (defined $taxon){
    $tree = Bio::Tree::Tree->new(-node => $taxon);
  } else {
    print "ERROR: Taxonid \"$taxid\" not found in taxonomy database.\n";
    next;
  }

  my @k_nodes = $tree->find_node(-rank => 'kingdom');
  my @p_nodes = $tree->find_node(-rank => 'phylum');
  my @c_nodes = $tree->find_node(-rank => 'class');
  my @o_nodes = $tree->find_node(-rank => 'order');
  my @f_nodes = $tree->find_node(-rank => 'family');
  my @g_nodes = $tree->find_node(-rank => 'genus');
  my @s_nodes = $tree->find_node(-rank => 'species');

  my @kingdom;
  my @phylum;
  my @class;
  my @order;
  my @family;
  my @genus;
  my @species;

  if (! @k_nodes){
    push (@kingdom, 'unidentified');
  } else {
    foreach my $key (0..$#k_nodes){
      my $kingdom = $k_nodes[$key]->node_name;
      push(@kingdom,$kingdom);
    }
  }
  if (! @p_nodes){
    push (@phylum, 'unidentified');
  } else {
    foreach my $key (0..$#p_nodes){
      my $phylum = $p_nodes[$key]->node_name;
      push(@phylum,$phylum);
    }
  }
  if (! @c_nodes){
    push (@class, 'unidentified');
  } else {
    foreach my $key (0..$#c_nodes){
      my $class = $c_nodes[$key]->node_name;
      push(@class,$class);
    }
  }
  if (! @o_nodes){
    push (@order, 'unidentified');
  } else {
    foreach my $key (0..$#o_nodes){
      my $order = $o_nodes[$key]->node_name;
      push(@order,$order);
    }
  }
  if (! @f_nodes){
    push (@family, 'unidentified');
  } else {
    foreach my $key (0..$#f_nodes){
      my $family = $f_nodes[$key]->node_name;
      push(@family,$family);
    }
  }
  if (! @g_nodes){
    push (@genus, 'unidentified');
  } else {
    foreach my $key (0..$#g_nodes){
      my $genus = $g_nodes[$key]->node_name;
      push(@genus,$genus);
    }
  }
  if (! @s_nodes){
    push (@species, 'unidentified');
  } else {
    foreach my $key (0..$#s_nodes){
      my $species = $s_nodes[$key]->node_name;
      push(@species,$species);
    }
  }
  my $lin = "k__" . join(':',@kingdom) . 
            "; p__" . join(':', @phylum) . 
            "; c__" . join(':', @class) . 
            "; o__" . join(':', @order) . 
            "; f__" . join(':', @family) . 
            "; g__" . join(':', @genus) . 
            "; s__" . join(':', @species);
  print OUT $taxid, "\t", $lin, "\n";
}
close READ;
close OUT;
