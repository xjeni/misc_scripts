#!/usr/bin/env perl
use strict;
use warnings;

# Prints to output file a list of all the taxon ids
# of fungal genus names in NCBI Taxonomy database

my $nodes_dmp = shift or die $!;
my $out_taxids = shift or die $!;
my $rank = shift or die $!;
my $group_taxonid = shift;
if (! defined $group_taxonid){
  $group_taxonid = '4751'; # fungi
}

open(READ, "<", $nodes_dmp) or die $!;
my $taxids = {"$group_taxonid" => 'yes'};
my $count = 0;

open(OUT, ">", $out_taxids) or die $!;
while (my $line = <READ>){
  $count++;
  if ($count % 5000 == 0){
    print $count . " " . localtime . "\n";
  }
  my @line_vals = split(/\t\|\t/, $line);
  my $rank = $line_vals[2];
  if ($rank eq 'species'){
    my $ancestor = $line_vals[1];
    my $taxid = $line_vals[0];
    if (isFungi($ancestor)){
      #print "FUNGI FOUND.\n";
      #print $line;
      print OUT $taxid, "\n";
    }
  }
}
close READ;
close OUT;








sub isInGroup{
  my $taxid = shift or die $!;

  ## return false at top of tree
  if ($taxid eq '1'){
    return 0;
  }

  ## return true if taxid is already found to be a fungi
  ## return false if taxid is already found to not be a fungi
  if ($$taxids{$taxid}){
    if ($$taxids{$taxid} eq 'yes'){
      return 1;
    } else {
      return 0;
    }
  }

  ## otherwise check if ancestor is a fungi
  my $line = `grep -m1 \"^$taxid\t\" $nodes_dmp`;
  if ($line eq ''){
    die "TAXONID NOT FOUND: $taxid\n";
  }
  my @line_vals = split(/\t\|\t/, $line);
  my $ancestor = $line_vals[1];
  if (isInGroup($ancestor)){
    ## add self to taxids and return true
    $$taxids{$taxid} = 'yes';
    return 1;
  } else {
    $$taxids{$taxid} = 'no';
    return 0;
  }
}

