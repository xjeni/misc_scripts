#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long;
use FindBin;

## VARIABLES
my $ncbi_tax_dir;
my $rank;
my $group_id;
my $out_file;
my $err_help = "To see help doc, use command: \"perl getNamesFromGroup.pl -h\"";
my $nodes_dmp;
my $names_dmp;

## SETUP WITH COMMANDLINE OPTIONS
&setup;
$nodes_dmp = "$ncbi_tax_dir/nodes.dmp";
$names_dmp = "$ncbi_tax_dir/names.dmp";
print "Using NCBI Taxonomy directory: $ncbi_tax_dir\n";
print "Output file path: $out_file\n";

## PRINT INFO
if (! defined $group_id){
  ## no group filter
  print "No group taxon id is provided. Group filter is off.\n";
  print "(Note: To set the group taxon id, use: -group [taxonid_of_group])\n";
} else {
  print "Getting names belonging to group with taxon id: $group_id\n";
}
if (! defined $rank){
  ## no rank filter
  print "No rank is provided. Rank filter is off.\n";
  print "(Note: To set the rank, use: -rank [rank_of_names])\n";
} else {
  print "Getting names with rank: $rank\n";
}
$nodes_dmp = "$ncbi_tax_dir/nodes.dmp";
$names_dmp = "$ncbi_tax_dir/names.dmp";

## START
print "START: " . localtime() . "\n";

print "Getting taxon ids...\n";
## GET TAXON ID
open(READ, "<", $nodes_dmp) or die $!;
my @output;
if (defined $group_id){
  my $taxids_checked = {"$group_id" => 'yes'};
  while (my $line = <READ>){
    my @line_vals = split(/\t\|\t/, $line);
    my $cur_rank = $line_vals[2];
    my $ancestor = $line_vals[1];
    my $taxid = $line_vals[0];
    if (defined $rank){
      if ($cur_rank eq $rank){
        if (isInGroup($ancestor, $taxids_checked)){
          ## Add to list if:
          ##    group id defined
          ##    rank defined
          ##    taxon id belongs to group
          ##    taxon id has the right rank
          push(@output, "$taxid\t$cur_rank");
        }
      }
    } else {
      if (isInGroup($ancestor, $taxids_checked)){
        ## Add to list if:
        ##    group id defined
        ##    rank not defined
        ##    taxon id belongs to group
        push(@output, "$taxid\t$cur_rank");
      }
    }
  }
} else {
  while (my $line = <READ>){
    my @line_vals = split(/\t\|\t/, $line);
    my $cur_rank = $line_vals[2];
    my $taxid = $line_vals[0];
    if (defined $rank){
      if ($cur_rank eq $rank){
        ## Add to list if:
        ##    group id not defined
        ##    rank defined
        ##    taxon id has the right rank
        push(@output, "$taxid\t$cur_rank");
      }
    } else {
      ## Add to list if:
      ##    group id not defined
      ##    rank not defined
      push(@output, "$taxid\t$cur_rank");
    }
  }
}
close READ;

## GET LINEAGE
print "Getting lineage & printing...\n";
open(OUT, ">", $out_file) or die $!;
foreach my $taxid_rank(@output){
  my $taxid = (split(/\t/, $taxid_rank))[0];
  my $rank = (split(/\t/, $taxid_rank))[1];
  my $name_line = `grep -m1 \"^$taxid\t.*scientific name\" $names_dmp`;
  my $name;
  if (defined $name_line){
    $name = (split(/\t\|\t/, $name_line))[1];
  } else {
    $name = "NO_SCIENTIFIC_NAME";
  }
  my $lin = getLineage($taxid, $ncbi_tax_dir);
  $lin = fixLineage($lin);
  print OUT $taxid, "\t", $name, "\t", $rank, "\t", $lin, "\n";
}
close OUT;

print "END: " . localtime() . "\n";

sub isInGroup{
  my $taxid = shift or die $!;  ## check if this particular tax id is in group
  my $taxids = shift or die $!;  ## hash of taxon ids already checked

  ## return false at top of tree
  if ($taxid eq '1'){
    return 0;
  }

  ## return true if taxid is already found to be a fungi
  ## return false if taxid is already found to not be a fungi
  if ($$taxids{$taxid}){
    if ($$taxids{$taxid} eq 'yes'){
      return 1;
    } else {
      return 0;
    }
  }

  ## otherwise check if ancestor is a fungi
  my $line = `grep -m1 \"^$taxid\t\" $nodes_dmp`;
  if ($line eq ''){
    die "TAXONID NOT FOUND: $taxid\n";
  }
  my @line_vals = split(/\t\|\t/, $line);
  my $ancestor = $line_vals[1];
  if (isInGroup($ancestor, $taxids)){
    ## add self to taxids and return true
    $$taxids{$taxid} = 'yes';
    return 1;
  } else {
    $$taxids{$taxid} = 'no';
    return 0;
  }
}

## Given a taxon id and a ncbi taxonomy directory path;
##   Return the lineage with
##   all the taxid's ancestors added in.
sub getLineage{
  my $taxid = shift or die $!;
  my $ncbi_tax_dir = shift or die $!;
  my $cur_lin = "";
  my $nodes_dmp = "$ncbi_tax_dir/nodes.dmp";
  my $names_dmp = "$ncbi_tax_dir/names.dmp";
  ## return lineage at the top of the tree
  if ($taxid eq '1'){
    return $cur_lin;
  }
  ## get current name
  my $name_line = `grep -m1 \"^$taxid\t.*scientific name\" $names_dmp`;
  chomp $name_line;
  if (!$name_line){
    return $cur_lin;
  }
  my $name = (split(/\t\|\t/, $name_line))[1];
  ## get current rank
  my $node_line = `grep -m1 \"^$taxid\t\" $nodes_dmp`;
  chomp $node_line;
  if (!$node_line){
    return $cur_lin;
  }
  my $rank = (split(/\t\|\t/, $node_line))[2];
  ## get ancestor's taxid
  my $ancestor_id = (split(/\t\|\t/, $node_line))[1];
  ## check if rank is one of the ones desired. If so, add to lineage.
  if (($rank eq 'kingdom') ||
      ($rank eq 'phylum') ||
      ($rank eq 'class') ||
      ($rank eq 'order') ||
      ($rank eq 'family') ||
      ($rank eq 'genus') ||
      ($rank eq 'species')){
    if ($cur_lin){
      $cur_lin = substr($rank, 0, 1) . "__$name" . "; " . $cur_lin;
    } else {
      $cur_lin = substr($rank, 0, 1) . "__$name";
    }
  }
  if ($rank eq 'kingdom'){
    return $cur_lin;
  }

  my $ancestor_lin = getLineage($ancestor_id, $ncbi_tax_dir);
  if (($cur_lin) &&
      ($ancestor_lin)){
    $cur_lin = $ancestor_lin . "; " . $cur_lin;
  } elsif ($ancestor_lin) {
    $cur_lin = $ancestor_lin;
  }
  
  return $cur_lin;
}

## Fix the format of the lineage string.
##   I.e. Add "c__unidentified" if it
##        is missing the class name.
sub fixLineage{
  my $lineage = shift or die $!;
  my $kingdom = "k__unidentified";
  my $phylum = "p__unidentified";
  my $class = "c__unidentified";
  my $order = "o__unidentified";
  my $family = "f__unidentified";
  my $genus = "g__unidentified";
  my $species = "s__unidentified";
  if ($lineage =~ /(k__[^;]+;)|(k__[^;]+$)/){
    if (defined $1){
      $kingdom = $1;
    } else {
      $kingdom = $2;
    }
    $kingdom =~ s/;//g;
  }
  if ($lineage =~ /(p__[^;]+;)|(p__[^;]+$)/){
    if (defined $1){
      $phylum = $1;
    } else {
      $phylum = $2;
    }
    $phylum =~ s/;//g;
  }
  if ($lineage =~ /(c__[^;]+;)|(c__[^;]+$)/){
    if (defined $1){
      $class = $1;
    } else {
      $class = $2;
    }
    $class =~ s/;//g;
  }
  if ($lineage =~ /(o__[^;]+;)|(o__[^;]+$)/){
    if (defined $1){
      $order = $1;
    } else {
      $order = $2;
    }
    $order =~ s/;//g;
  }
  if ($lineage =~ /(f__[^;]+;)|(f__[^;]+$)/){
    if (defined $1){
      $family = $1;
    } else {
      $family = $2;
    }
    $family =~ s/;//g;
  }
  if ($lineage =~ /(g__[^;]+;)|(g__[^;]+$)/){
    if (defined $1){
      $genus = $1;
    } else {
      $genus = $2;
    }
      $genus =~ s/;//g;
  }
  if ($lineage =~ /(s__[^;]+;)|(s__[^;]+$)/){
    if (defined $1){
      $species = $1;
    } else {
      $species = $2;
    }
    $species =~ s/;//g;
  }

  return $kingdom . "; " .
         $phylum . "; " .
         $class . "; " .
         $order . "; " .
         $family . "; " .
         $genus . "; " .
         $species;
}


## SETUP
sub setup{
  my $help;
  GetOptions("--help" => \$help, 
             "-help" => \$help, 
             "-h" => \$help, 
             "-group=s" => \$group_id,
             "-out=s" => \$out_file,
             "-rank=s" => \$rank,
             "-tax_dir=s" => \$ncbi_tax_dir) or die $!;
  if (defined $help){
    # prints out the help doc
    system("cat $FindBin::Bin/help_getNamesFromGroup.txt");
    exit;
  }

  my $err = 0;

  ## Checking if mandatory fields are given or valid
  if (! defined $ncbi_tax_dir){
    print "Error: the NCBI taxonomy directory is not provided. (-tax_dir [NCBI_Taxonomy_path])\n";
    $err = 1;
  } elsif (! -d $ncbi_tax_dir){
    print "Error: the provided NCBI taxonomy directory does not exist.\n";
    print "    Input: $ncbi_tax_dir\n";
    $err = 1;
  }
  if (! defined $out_file){
    print "Error: the output file path is not provided. (-out [output_file_path])\n";
    $err = 1;
  }
  if ($err){
    print "($err_help)\n";
    exit;
  }
}
