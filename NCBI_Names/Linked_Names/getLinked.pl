#!/usr/bin/env perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/Modules/Text-Unaccent-PurePerl-0.05/lib";
use Text::Unaccent::PurePerl qw(unac_string);
use Getopt::Long;

## Given a list of names, obtain all the linked names for it
## For names that appear multiple times in names.dmp, print out both
## The script will print out a column of lineage, to make sure the
##   the name is the one that the user wanted


## VARIABLES
my $ncbi_tax_dir;
my $names_file;
my $out_file;
my $kingdom;
my $not_found_file;
my $err_help = "To see help doc, use command: \"perl getLinked.pl -h\"";
my $output_header = "NAME" . "\t" .
                    "TAXID" . "\t" .
                    "CLASS" . "\t" .
                    "LINEAGE" . "\t" .
                    "LINKED_NAMES";

## SETUP VARIABLE VALUES FROM COMMANDLINE OPTIONS
&setup;

## SETUP DEFAULT VARIABLES IF NOT DEFINED
print "Using NCBI taxonomy directory: $ncbi_tax_dir\n";
print "Using output file path: $out_file\n";
print "Using names file: $names_file\n";
if (! defined $not_found_file){
  $not_found_file = 'names_not_found.txt';
  print "Output path for list of not found names is not provided. Using default path: $not_found_file\n";
  print "(Note: To provide a different file path, use: -not_found [output_file_path])\n";
} else {
  print "Names not found file: $not_found_file\n";
}
if (defined $kingdom){
  print "Finding only the names that belong to kingdom: $kingdom\n";
} else {
  print "No kingdom name is provided. Keeping all entries found.\n";
  print "(Note: To provide a kingdom, use: -kingdom [kingdom_name])\n";
  $kingdom = 'N\A';
}


## START
print "START: " . localtime . "\n";

open(READ, "<", $names_file) or die $!;
open(OUT, ">", $out_file) or die $!;
print OUT $output_header, "\n";
open(NOT_FOUND, ">", $not_found_file) or die $!;
while (my $name = <READ>){
  chomp $name;
  next if ($name eq '');
  my $name_fixed = unac_string($name);
  $name_fixed =~ s/\R//g;
  print "Finding linked names for \"$name_fixed\"...\n";
  my $output = &findLinked($name_fixed, 
                           $kingdom, 
                           $ncbi_tax_dir);

  if (scalar @$output){
    foreach my $line(@$output){
      print OUT $line, "\n";
    }
  } else {
    print $name_fixed . " is not found.\n";
    print NOT_FOUND $name_fixed, "\n";
  }
}
close NOT_FOUND;
close READ;
close OUT;

print "END: " . localtime . "\n";
## END





# SETUP OPTIONS AND VARIABLE VALUES
sub setup{
  my $help;
  GetOptions("--help" => \$help, 
             "-help" => \$help, 
             "-h" => \$help, 
             "-tax_dir=s" => \$ncbi_tax_dir,
             "-out=s" => \$out_file,
             "-names=s" => \$names_file,
             "-not_found=s" => \$not_found_file,
             "-kingdom=s" => \$kingdom) or die $!;
  if (defined $help){
    # prints out the help doc
    system("cat $FindBin::Bin/help_getLinked.txt");
    exit;
  }

  my $err = 0;

  ## Checking if mandatory fields are given
  if (! defined $ncbi_tax_dir){
    print "Error: the NCBI taxonomy directory is not provided. (-tax_dir [NCBI_Taxonomy_path])\n";
    $err = 1;
  } elsif (! -d $ncbi_tax_dir){
    print "Error: the provided NCBI Taxonomy directory path does not exist.\n";
    print "    Input: $ncbi_tax_dir\n";
    $err = 1;
  }
  if (! defined $out_file){
    print "Error: output directory path is not provided. (-out [output_file])\n";
    $err = 1;
  }
  if (! defined $names_file){
    print "Error: names file is not provided. (-names [names_file_path])\n";
    $err = 1;
  } elsif (! -e $names_file){
    print "Error: the provided names file does not exist.\n";
    print "    Input: $names_file\n";
    $err = 1;
  }
 
  # If input errors found, then exit script
  if ($err){
    print "($err_help)\n";
    exit;
  }
}

## Given a name, a kingdom that the name belongs to,
##    a nodes.dmp file path, and a names_dmp file path,
## Find all the linked names for that particular name.
sub findLinked{
  my $name = shift or die $!;
  my $kingdom = shift or die $!;
  my $ncbi_tax_dir = shift or die $!;
  my $names_dmp = "$ncbi_tax_dir/names.dmp";
  my $nodes_dmp = "$ncbi_tax_dir/nodes.dmp";
  my @lines  = `grep \'|\t$name\t|\' $names_dmp`;
  my @output;
  if (scalar @lines == 0){
    # If name is not found in dmp
    #   return empty array
    return \@output;
  }

  foreach my $line(@lines){
    my $output_str;
    my $linked_str = "";
    my @line_vals = split(/\t*\|\t*/, $line);
    my $taxid = $line_vals[0];
    ## get lineage
    my $lineage = getLineage($taxid, $ncbi_tax_dir);
    ## fix lineage (put '__unidentified' for the ranks not found)
    $lineage = fixLineage($lineage);
    ## If a kingdom is provided, check if kingdom matches
    if (($kingdom ne 'N\A') &&
        ("k__$kingdom" ne 
         (split(/; /, $lineage))[0])){
      ## name is not from the right kingdom; move to next one
      next;
    } 
    my $name = $line_vals[1];
    my $class = $line_vals[3];
    my $grep_names = "grep \"^$taxid\t\" $names_dmp |" . 
                     "grep -v \"^$taxid\t|\t$name\t|\"";

    my @linked_lines = `$grep_names`;

    $output_str = $name . "\t" . 
                     $taxid . "\t" .
                     $class . "\t" .
                     $lineage;
    foreach my $linked_line (@linked_lines){
      my @linked_vals = split(/\t*\|\t*/, $linked_line);
      my $linked_name = $linked_vals[1];
      my $linked_class = $linked_vals[3];
      my $cur_linked_str = $linked_name . "|" .
                           $linked_class . "||";
      $linked_str = $linked_str . $cur_linked_str;
    }
    if ($linked_str ne ""){
      ## removing the extra '||' at the end
      chop $linked_str;
      chop $linked_str;
    }
    $output_str = $output_str . "\t" .
                  $linked_str;
    push(@output, $output_str);
  }
  return \@output;
}


## Given a taxon id and a ncbi taxonomy directory path;
##   Return the lineage with
##   all the taxid's ancestors added in.
sub getLineage{
  my $taxid = shift or die $!;
  my $ncbi_tax_dir = shift or die $!;
  my $cur_lin = "";
  my $nodes_dmp = "$ncbi_tax_dir/nodes.dmp";
  my $names_dmp = "$ncbi_tax_dir/names.dmp";
  ## return lineage at the top of the tree
  if ($taxid eq '1'){
    return $cur_lin;
  }
  ## get current name
  my $name_line = `grep -m1 \"^$taxid\t.*scientific name\" $names_dmp`;
  chomp $name_line;
  if (!$name_line){
    die "ERROR: NAME NOT FOUND: $taxid\n";
  }
  my $name = (split(/\t\|\t/, $name_line))[1];
  ## get current rank
  my $node_line = `grep -m1 \"^$taxid\t\" $nodes_dmp`;
  chomp $node_line;
  if (!$node_line){
    die "ERROR: NODE NOT FOUND: $taxid\n";
  }
  my $rank = (split(/\t\|\t/, $node_line))[2];
  ## get ancestor's taxid
  my $ancestor_id = (split(/\t\|\t/, $node_line))[1];
  ## check if rank is one of the ones desired. If so, add to lineage.
  if (($rank eq 'kingdom') ||
      ($rank eq 'phylum') ||
      ($rank eq 'class') ||
      ($rank eq 'order') ||
      ($rank eq 'family') ||
      ($rank eq 'genus') ||
      ($rank eq 'species')){
    if ($cur_lin){
      $cur_lin = substr($rank, 0, 1) . "__$name" . "; " . $cur_lin;
    } else {
      $cur_lin = substr($rank, 0, 1) . "__$name";
    }
  }
  if ($rank eq 'kingdom'){
    return $cur_lin;
  }

  my $ancestor_lin = getLineage($ancestor_id, $ncbi_tax_dir);
  if (($cur_lin) &&
      ($ancestor_lin)){
    $cur_lin = $ancestor_lin . "; " . $cur_lin;
  } elsif ($ancestor_lin) {
    $cur_lin = $ancestor_lin;
  }
  
  return $cur_lin;
}

## Fix the format of the lineage string.
##   I.e. Add "c__unidentified" if it
##        is missing the class name.
sub fixLineage{
  my $lineage = shift or die $!;
  my $kingdom = "k__unidentified";
  my $phylum = "p__unidentified";
  my $class = "c__unidentified";
  my $order = "o__unidentified";
  my $family = "f__unidentified";
  my $genus = "g__unidentified";
  my $species = "s__unidentified";
  if ($lineage =~ /(k__[^;]+;)|(k__[^;]+$)/){
    if (defined $1){
      $kingdom = $1;
    } else {
      $kingdom = $2;
    }
    $kingdom =~ s/;//g;
  }
  if ($lineage =~ /(p__[^;]+;)|(p__[^;]+$)/){
    if (defined $1){
      $phylum = $1;
    } else {
      $phylum = $2;
    }
    $phylum =~ s/;//g;
  }
  if ($lineage =~ /(c__[^;]+;)|(c__[^;]+$)/){
    if (defined $1){
      $class = $1;
    } else {
      $class = $2;
    }
    $class =~ s/;//g;
  }
  if ($lineage =~ /(o__[^;]+;)|(o__[^;]+$)/){
    if (defined $1){
      $order = $1;
    } else {
      $order = $2;
    }
    $order =~ s/;//g;
  }
  if ($lineage =~ /(f__[^;]+;)|(f__[^;]+$)/){
    if (defined $1){
      $family = $1;
    } else {
      $family = $2;
    }
    $family =~ s/;//g;
  }
  if ($lineage =~ /(g__[^;]+;)|(g__[^;]+$)/){
    if (defined $1){
      $genus = $1;
    } else {
      $genus = $2;
    }
      $genus =~ s/;//g;
  }
  if ($lineage =~ /(s__[^;]+;)|(s__[^;]+$)/){
    if (defined $1){
      $species = $1;
    } else {
      $species = $2;
    }
    $species =~ s/;//g;
  }

  return $kingdom . "; " .
         $phylum . "; " .
         $class . "; " .
         $order . "; " .
         $family . "; " .
         $genus . "; " .
         $species;
}
