#!/usr/bin/env perl
use strict;
use warnings;
use FindBin;
use Getopt::Long;

## Make a fasta file for all the sequences in UNITE for a particular taxonomic name
## One fasta file created in $out_path for each name

my $unite_fasta;
my $out_path;
my $names;
my $rank;
my $rank_letter;
my $err_help = "To see help doc, use command: \"perl extractSeqs.pl -h\"";

&setup;
print "Using UNITE fasta: $unite_fasta\n";
print "Using output path: $out_path\n";
print "Using names file: $names\n";
print "Rank of the names: $rank\n";

print "START: " . localtime . "\n";

open(READ, "<", $names) or die $!;
while (my $name = <READ>){
  print $name;
  chomp $name;
  next if ($name eq '');
  my $file_name = $name . ".fasta";
  $file_name =~ s/ /\_/g;
  open(OUT, ">", "$out_path/$file_name") or die $!;
  open(UNITE, "<", $unite_fasta) or die $!;
  while (my $unite_line = <UNITE>){
    if (substr($unite_line, 0, 1) eq '>'){
      if (($unite_line =~ /$rank_letter\__$name$/) ||
          ($unite_line =~ /$rank_letter\__$name\|/) ||
          ($unite_line =~ /$rank_letter\__$name;/)){
        print OUT $unite_line;
        $unite_line = <UNITE>;
        print OUT $unite_line;
      }
    }
  }
  close UNITE;
  close OUT;
  if (-z "$out_path/$file_name"){
    print STDERR "Warning: $name not found in UNITE fasta.\n";
    system("rm $out_path/$file_name");
  }
}
close READ;

print "END: " . localtime . "\n";

# SETUP OPTIONS AND DEFAULT VALUES
sub setup{
  my $help;
  GetOptions("--help" => \$help, 
             "-help" => \$help, 
             "-h" => \$help, 
             "-unite=s" => \$unite_fasta,
             "-out=s" => \$out_path,
             "-names=s" => \$names,
             "-rank=s" => \$rank) or die $!;
  if (defined $help){
    # prints out the help doc
    system("cat $FindBin::Bin/help.txt");
    exit;
  }

  my $err = 0;

  ## Checking if mandatory fields are given
  if (! defined $unite_fasta){
    print "Error: unite fasta path is not provided. (-unite [unite fasta path])\n";
    $err = 1;
  } elsif (! -e $unite_fasta){
    print "Error: the provided unite fasta path does not exist.\n";
    print "    Input: $unite_fasta\n";
    $err = 1;
  }
  if (! defined $out_path){
    print "Error: output directory path is not provided. (-out [output dir])\n";
    $err = 1;
  } elsif (! -d $out_path){
    print "Error: the provided output directory path does not exist.\n";
    print "    Input: $out_path\n";
    $err = 1;
  }
  if (! defined $names){
    print "Error: names file is not provided. (-names [names file path])\n";
    $err = 1;
  } elsif (! -e $names){
    print "Error: the provided names file does not exist.\n";
    print "    Input: $names\n";
    $err = 1;
  }
  if (! defined $rank){
    print "Error: the rank is not provided. (-rank [rank of names])\n";
    $err = 1;
  } elsif (($rank ne 'kingdom') &&
           ($rank ne 'phylum') &&
           ($rank ne 'class') &&
           ($rank ne 'order') &&
           ($rank ne 'family') &&
           ($rank ne 'genus') &&
           ($rank ne 'species') &&
           ($rank ne 'k') &&
           ($rank ne 'p') &&
           ($rank ne 'c') &&
           ($rank ne 'o') &&
           ($rank ne 'f') &&
           ($rank ne 'g') &&
           ($rank ne 's')){
    print "Error: the rank provided is invalid. See help doc for the list of valid inputs.\n";
    $err = 1;
  }
  # If input errors found, then exit script
  if ($err){
    print "($err_help)\n";
    exit;
  }
  $rank_letter = substr($rank, 0, 1);
}
